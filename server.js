const { validate: uuidValidate, v4: uuidv4 } = require('uuid');
const { generateKeyPair } = require('crypto');
const { default: SignJWT } = require('jose/jwt/sign')

var cookieParser = require('cookie-parser')
const express = require('express')
var cors = require('cors')
const app = express()
const port = process.env.PORT || 3000;
const dbUrl = process.env.DBURL || 'mongodb://localhost/authentication-service';

const mongoose = require('mongoose');
mongoose.connect(dbUrl, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true,});

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log(`Connected to db`)
});

const UserModel = new mongoose.model("user", {
  username: { type: String, unique: true, required: true },
  password: { type: String, required: true },
  refreshTokens: [{
    deviceID: { type: String },
    token: { type: String }
  }]
});

var publicKey;
var privateKey;

generateKeyPair('ec', {
  namedCurve: 'P-256',
  publicKeyEncoding: {
    type: 'spki',
    format: 'pem'
  },
}, (err, pubKey, privKey) => {
  publicKey = pubKey;
  privateKey = privKey;
});

async function generateAccessToken (username) {
  try {
    const jwt = await new SignJWT({
      'sub': username
    })
    .setProtectedHeader({ alg: 'ES256' })
    .setIssuedAt()
    .setIssuer('sepia.co:auth')
    .setAudience('sepia.co:media')
    .setExpirationTime('15m')
    .sign(privateKey)

    return jwt;
  } catch (e) {
    throw e;
  }
}

function updateUserTokens (user, reqDeviceID, newRefreshToken) {
  const tempUser = user;
  const tokens = user.refreshTokens.map(el => el.deviceID);
  const matchIndex = tokens.indexOf(reqDeviceID);

  //make sure device doesn't already have a refresh token
  if (matchIndex === -1) {
    tempUser.refreshTokens.push({
      deviceID: reqDeviceID,
      token: newRefreshToken
    })
  } else {
    //update existing refresh token if one exists
    tempUser.refreshTokens[matchIndex] = {
      deviceID: reqDeviceID,
      token: newRefreshToken
    }
  }

  return (tempUser);
}

app.use(express.json())
app.use(cookieParser())

app.use(cors())

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/getPublicKey', (req, res) => {
  res.send(publicKey)
})

app.post('/signup', function (req, res) {
  let newUser = new UserModel({
    username: req.body.username,
    password: req.body.password
  })

  //Look for an existing user with same username to avoid duplicates
  UserModel.findOne({ username: newUser.username }, function (err, user) {
    if (err) {
      console.log("/signup: db search failed")
      console.log("err: " + err)
      res.status(500).end();
    }

    if (user === null) {
      const newRefreshToken = uuidv4();
      newUser = updateUserTokens(newUser, req.body.deviceID);

      newUser.save(function (err, newUser) {
        if (err) {
          console.log("/signup: failed adding new user to db")
          console.log("err: " + err)
          res.status(500).end();
        }

        generateAccessToken(newUser.username).then( accessToken => {
          res.cookie('refreshToken', newRefreshToken, { httpOnly: true, secure: true, sameSite: "strict" });
          res.cookie('accessToken', accessToken, { httpOnly: true, secure: true, sameSite: "strict" });
          res.status(201).end();
        }).catch(err => {
          if (err) {
            console.log("/signup: failed to generate access token")
            console.log("err: " + err)
            res.status(500).end();
          }
        })
      });
    } else {
      res.status(409).send({ message: "user already exists" })
    }
  });
})

app.post('/login', function (req, res) {
  const reqParsed = {
    username: req.body.username,
    password: req.body.password,
    deviceID: req.body.deviceID
  }

  if (!uuidValidate(reqParsed.deviceID)) res.status(400).send({ message: "invalid device id" })

  UserModel.findOne({ username: reqParsed.username }, function (err, user) {
    if (err) {
      console.log("/login: db search failed")
      console.log("err: " + err)
      res.status(500).end();
    }

    //check username
    if (user === null) {
      res.status(401).send({ message: "user doesn't exist" })
    }
    //check password
    else if (reqParsed.password === user.password) {
      const newRefreshToken = uuidv4();
      const updatedUser = updateUserTokens(user, req.body.deviceID);

      //save updates to database
      updatedUser.save(function (err, updatedUser) {
        if (err) {
          console.log("/login: failed updating user in db")
          console.log("err: " + err)
          res.status(500).end();
        }

        generateAccessToken(reqParsed.username).then( accessToken => {
          res.cookie('refreshToken', newRefreshToken, { httpOnly: true, secure: true, sameSite: "strict" });
          res.cookie('accessToken', accessToken, { httpOnly: true, secure: true, sameSite: "strict" });
          res.status(200).end();
        }).catch(err => {
          if (err) {
            console.log("/login: failed to generate access token")
            console.log("err: " + err)
            res.status(500).end();
          }
        })
      });
    } else {
      res.status(401).send({ message: "wrong password" })
    }
  });
})

app.post('/refresh', function (req, res) {
  const reqParsed = {
    username: req.body.username,
    deviceID: req.body.deviceID,
    refreshToken: req.cookies.refreshToken
  }

  if (reqParsed.refreshToken) {
    UserModel.findOne({ username: reqParsed.username }, function (err, user) {
      if (err) res.status(500).end();

      //check username
      if (user === null) {
        res.status(401).send({ message: "user doesn't exist" })
      } else {
        const tokens = user.refreshTokens.map(el => el.deviceID);
        const matchIndex = tokens.indexOf(reqParsed.deviceID);

        if (matchIndex === -1) {
          res.status(401).send({ message: "device not logged in" })
        } else {
          if (reqParsed.refreshToken === user.refreshTokens[matchIndex].token){
            generateAccessToken(reqParsed.username).then( accessToken => {
              res.cookie('accessToken', accessToken, { httpOnly: true, secure: true, sameSite: "strict" });
              res.status(200).end();
            }).catch(err => {
              res.status(500).send(err);
            })
          } else {
            res.status(401).send({ message: "invalid refresh token" })
          }
        }
      }
    })
  } else {
    res.status(401).send({ message: "missing refresh token" })
    console.log(req.cookies)
  }
})

app.post('/logout', function (req, res) {
  const reqParsed = {
    username: req.body.username,
    deviceID: req.body.deviceID,
    refreshToken: req.cookies.refreshToken
  }

  if (reqParsed.refreshToken) {
    UserModel.findOne({ username: reqParsed.username }, function (err, user) {
      if (err) res.status(500).end();

      //check username
      if (user === null) {
        res.status(401).send({ message: "user doesn't exist" })
      } else {
        const tokens = user.refreshTokens.map(el => el.deviceID);
        const matchIndex = tokens.indexOf(reqParsed.deviceID);

        if (matchIndex === -1) {
          res.status(401).send({ message: "device not logged in" })
        } else {
          if (reqParsed.refreshToken === user.refreshTokens[matchIndex].token){
            const tempUser = user;
            tempUser.refreshTokens.splice(matchIndex, 1)

            tempUser.save(function (err, updatedUser) {
                if (err) res.status(500).send(err);

                res.clearCookie('refreshToken');
                res.clearCookie('accessToken');
                res.status(200).end()
            })
          } else {
            res.status(401).send({ message: "invalid refresh token" })
          }
        }
      }
    })
  } else {
    res.status(401).send({ message: "missing refresh token" })
    console.log(req.cookies)
  }
})

// Insecure test endpoints
/*app.post('/deleteUser', function (req, res) {
  UserModel.deleteOne({ username: req.body.username }, function (err) {
    if (err) res.status(500).end();
  });

  res.status(200).end();
})

app.get('/getUsers', (req, res) => {
  UserModel.find(function (err, users) {
    if (err) return console.error(err);
    res.json(users);
  })
})*/

app.listen(port, () => {
  console.log(`App listening at port ${port}`)
})
